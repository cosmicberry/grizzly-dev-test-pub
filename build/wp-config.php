<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'grizzly_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!q5!scPeOoT}),:7Ah.W@rB`2OQ|tUBxxvu5`#PJl8dO$L,x]xMEYE6,nh8ye2Jb');
define('SECURE_AUTH_KEY',  'CXzoS=[#$9D0}9>D7wFd4r5ymdJk/mjbawM33{cLJO~KC~bTvL0:;x7)DKNV;T_t');
define('LOGGED_IN_KEY',    '8+=NgCQLQy<R,[@+bTr*b?3)/A0~f$s6-_6|grD;NU*o&%<VIj>1d0!H5PNf-=&{');
define('NONCE_KEY',        'p6KCI}b`Xr+tzm^H:~5Y<D?`KXAO?pt.:V<,@wa5+S> 0d5xW3O#D),IiH@qB0TW');
define('AUTH_SALT',        'lPK&!K25GGo:v{Y=u0*rc|>qb3.B<n],kRFDT7hs%gaMWyE?q@!nzEe+N6N(,4gJ');
define('SECURE_AUTH_SALT', '|:2 I@ /jzF9^ hqj,s$mY].l_+ow7VK6q|XLUwz0Y#WlV925,f9R:uQ~muVX?(:');
define('LOGGED_IN_SALT',   'uJbPin@3B90NJ(;kKIpCK?/|lAGIAz]!}UD2p@+P3ibeDShxEJy^!pD%Xvva6FY(');
define('NONCE_SALT',       'O8h(LUX]iumPG&{k)FDdoe/{h9MgkKT),&|iLpS^b3wg4g;f2L;7P@Az.)f{NpS3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
