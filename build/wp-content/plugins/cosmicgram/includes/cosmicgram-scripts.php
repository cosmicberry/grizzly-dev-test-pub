<?php

	function ipl_add_scripts() {
		wp_enqueue_style( 'ipl-style', plugins_url() . '/cosmicgram/css/styles.css' );
		wp_enqueue_script( 'ipl-style', plugins_url() . '/cosmicgram/js/main.js', array( 'jquery' ) );
	}

	add_action( 'wp_enqueue_scripts', 'ipl_add_scripts' );