<?php

	function ipl_options_menu_link(){
		add_options_page(
			'Instagram Photo List Options',
			'Instagram Photo List',
			'manage_options',
			'ipl-options',
			'ipl_options_content'
		);
	}

	function ipl_options_content(){

		global $ipl_options;

		$redirect_url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

		?>
		
		<div class="wrap">
			<h2>Instagram Photo List Settings</h2>
			<p>Settings for the IPL plugin</p>
			<form action="options.php" method="post">
				<?php settings_fields('ipl_settings_group'); ?>
				<table class="form-table">
					<tbody>
						<tr>
							<th scope="row"><label for="ipl_settings[redirect_url]"><?php _e('Redirect URL'); ?></label></th>
							<td>
								<input type="text" name="ipl_settings[redirect_url]" class="regular-text" id="ipl_settings[redirect_url]" value="<?php echo $redirect_url; ?>" disabled >
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="ipl_settings[client_id]"><?php _e('Client ID'); ?></label></th>
							<td>
								<input type="text" name="ipl_settings[client_id]" class="regular-text" id="ipl_settings[client_id]" value="<?php echo $ipl_options['client_id']; ?>" >
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="authenticate"><?php _e('Authenticate'); ?></label></th>
							<td>
								<a href="https://api.instagram.com/oauth/authorize/?client_id=<?php echo $ipl_options['client_id'] ?>&redirect_uri=<?php echo $redirect_url; ?>&response_type=token&scope=public_content" class="button btn">Authenticate</a>
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="ipl_settings[access_token]"><?php _e('Access Token'); ?></label></th>
							<td>
								<input type="text" name="ipl_settings[access_token]" class="regular-text" id="ipl_settings[access_token]" value="<?php echo $ipl_options['access_token']; ?>" >
							</td>
						</tr>
					</tbody>
				</table>
				<p class="submit"><input type="submit" name="submit" id="submit" class="button" value="<?php _e('Save Changes'); ?>"></p>
			</form>
		</div>
		
		<?php
	}

	add_action('admin_menu', 'ipl_options_menu_link');

	function ipl_register_settings(){
		register_setting('ipl_settings_group', 'ipl_settings');
	}

	add_action('admin_init', 'ipl_register_settings');