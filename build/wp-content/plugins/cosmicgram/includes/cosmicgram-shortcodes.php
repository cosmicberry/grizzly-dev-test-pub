<?php

	function ipl_list_photos( $atts, $content = null ) {
		global $ipl_options;

		$atts = shortcode_atts( array(
			'title' => 'Instagram Photo List',
			'count' => 8
		), $atts );

		$url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $ipl_options['access_token'] . '&count=' . $atts['count'];


		$options  = array(
			'http' => array(
				'user_agent' => $_SERVER['HTTP_USER_AGENT']
			)
		);
		$context  = stream_context_create( $options );
		$response = file_get_contents( $url, false, $context );

		$data = json_decode( $response )->data;

		$output = '<div class="owl-carousel">';

		foreach ( $data as $photo ) {
			$img = $photo->images->standard_resolution->url;
			$output .= '<div class="item" style="background-image: url(' . $img . ');"></div>';
		}
		$output .= '</div>';


		/*
		 *
		 * <div class="owl-item">
				<div class="item">
					<h4>1</h4>
				</div>
			</div>
		 *
		 *
		 */

		return $output;

	}

	add_shortcode( 'cosmicgram', 'ipl_list_photos' );