<?php
	/**
	 * Plugin Name: Cosmicgram
	 * Description: Show latest Instagram photos
	 * Version: 1.0
	 * Author: Jeremy "Jermbo" Lawson
	 * Author URI: http://cosmicstrawberry.com
	 *
	 */

	if(!defined('ABSPATH')){
		exit;
	}

	$ipl_options = get_option('ipl_settings');

	require_once( plugin_dir_path(__FILE__) . '/includes/cosmicgram-scripts.php');
	require_once( plugin_dir_path(__FILE__) . '/includes/cosmicgram-shortcodes.php');

	if( is_admin() ){
		require_once( plugin_dir_path(__FILE__) . '/includes/cosmicgram-settings.php');
	}