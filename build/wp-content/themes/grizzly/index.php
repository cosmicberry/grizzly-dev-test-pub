<?php get_header(); ?>

<header>
	<div class="container">
		<div class="col-xs-12">

			<h1 class="logo"><? bloginfo( 'title' ); ?></h1>
			<a href="mailto:info@grizzly.com" class="fa fa-envelope-o contact-us">Contact Us</a>

		</div>
	</div>
</header>


<header class="wrapper wrapper--top">
	<div class="container">

		<div class="col-xs-12 col-md-10 col-lg-8 col-xl-6">
			<p class="content__text content__text--large">Are you ready for your next adventure? We’re always looking for top talent to join our team in sunny San Diego.</p>
		</div>

	</div><!-- /content -->
</header><!-- /wrapper--top -->

<div class="wrapper wrapper--contact">
	<div class="container">

		<div class="col-xs-12 col-md-9 col-xl-7">

			<p class="title title--small">Is Grizzly right for you?</p>

			<p class="content__text content__text--medium">If you ‘d like to be considered for a position here at Grizzly,
				please send us an email using the form below.</p>

			<?php echo do_shortcode('[contact-form-7 id="4" title="Main Contact Form"]'); ?>

		</div>

	</div><!-- /container -->
</div><!-- /wrapper--contact -->

<div class="wrapper wrapper--instagram">
	<?php echo do_shortcode( '[cosmicgram]' ); ?>
</div>

<script>
	jQuery( function ( $ ) {
		$( '.owl-carousel' ).owlCarousel( {
			loop            : true,
			responsiveClass : true,
			responsive      : {
				0    : {
					items : 2,
					nav   : true
				},
				600  : {
					items : 5,
					nav   : false
				},
				1000 : {
					items : 5,
					nav   : true,
					loop  : true
				}
			}
		} )

	} )
</script>


<footer class="wrapper wrapper--footer">
	<div class="container">
		<div class="col-xs-12 col-md-6">
			<div class="footer-slogan">
				<img src="<?= get_stylesheet_directory_uri(); ?>/images/grizzly-logo.svg" alt="" style="width: 25px;">
				<p class="slogan">Long Live the Adventure&trade;</p>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="footer-socials">
				<a href="#" class="social">Instagram</a>
				<a href="#" class="social">Facebook</a>
				<a href="#" class="social">Twitter</a>
			</div>
		</div>
	</div>
</footer>

<?php get_footer(); ?>