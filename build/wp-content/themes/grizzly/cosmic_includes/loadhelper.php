<?php
	define('WP_USE_THEMES', false);
	require_once('../../../../wp-load.php');

	$numPosts = (isset($_GET['numPosts'])) ? $_GET['numPosts'] : 3;
	$page = (isset($_GET['pageNumber'])) ? $_GET['pageNumber'] : 0;

	query_posts(array(
		'posts_per_page' => $numPosts,
		'paged' => $page
	));

	if( have_posts() ):while(have_posts()): the_post(); ?>

		<div class="col-xs-12 col-xs-4">
			<article class="c-blog">
				<h1 class="c-blog__title"><?php the_title(); ?></h1>
				<h2 class="c-blog__meta"><?php echo the_date(); ?> | By : <span><?php the_author(); ?></span></h2>
				<div class="c-blog__content">
					<?php the_excerpt(); ?>
					<!-- <a href="<?php the_permalink(); ?>" class="c-btn c-btn--block">Read More</a> -->
				</div>
			</article>
		</div>


	<?php	endwhile; endif;

	wp_reset_query();