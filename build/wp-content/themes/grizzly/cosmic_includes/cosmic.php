<?php
	/**
	 * Welcome to Cosmic WordPress
	 *
	 * This is the core Cosmic WordPress file where most of the magic happens.
	 *
	 *
	 * Developed by: Cosmic Strawberry
	 * URL: http://cosmicstrawberry.com
	 *
	 * - head cleanup
	 *
	 */


	/**
	 * The default WordPress head is a mess.
	 * Let's clean it up by removing all the junk we don't need.
	 *
	 * [1] - EditURI link
	 * [2] - windows live writer
	 * [3] - previous link
	 * [4] - start link
	 * [5] - links for adjacent posts
	 * [6] - WP version
	 * [7] - remove WP version from css
	 * [8] - remove WP version from scripts
	 */
	function head_cleanup() {
		remove_action( 'wp_head', 'rsd_link' ); // [1]
		remove_action( 'wp_head', 'wlwmanifest_link' ); // [2]
		remove_action( 'wp_head', 'parent_post_rel_link', 10 ); // [3]
		remove_action( 'wp_head', 'start_post_rel_link', 10 ); // [4]
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 ); // [5]
		remove_action( 'wp_head', 'wp_generator' ); // [6]
		add_filter( 'style_loader_src', 'remove_wp_ver_css_js', 9999 ); // [7]
		add_filter( 'script_loader_src', 'remove_wp_ver_css_js', 9999 ); // [8]
	}

	// remove WP version from RSS
	function rss_version() {
		return '';
	}

	// remove WP version from scripts
	function remove_wp_ver_css_js( $src ) {
		if ( strpos( $src, 'ver=' ) ) {
			$src = remove_query_arg( 'ver', $src );
		}

		return $src;
	}

	function theme_support() {
		// wp thumbnails (sizes handled in functions.php)
		add_theme_support( 'post-thumbnails' );

		// wp custom background (thx to @bransonwerner for update)
		//add_theme_support( 'custom-background',
		//  array(
		//    'default-image' => '',    // background image default
		//    'default-color' => '',    // background color default (dont add the #)
		//    'wp-head-callback' => '_custom_background_cb',
		//    'admin-head-callback' => '',
		//    'admin-preview-callback' => ''
		//  )
		//);

		// rss thingy
		add_theme_support( 'automatic-feed-links' );

		// to add header image support go here: http://themble.com/support/adding-header-background-image-support/

		// adding post format support
		//add_theme_support( 'post-formats',
		// array(
		//    'aside',             // title less blurb
		//    'gallery',           // gallery of images
		//    'link',              // quick link to other site
		//    'image',             // an image
		//    'quote',             // a quick quote
		//    'status',            // a Facebook like status update
		//    'video',             // video
		//    'audio',             // audio
		//    'chat'               // chat transcript
		//  )
		//);

		// wp menus
		add_theme_support( 'menus' );

		// registering wp3+ menus
		//register_nav_menus(
		//	array(
		//		'main-nav'     => 'The Main Menu',
		//		'footer-links' => 'Footer Links'
		//	)
		//);

		// Enable support for HTML5 markup.
		add_theme_support( 'html5', array(
			'comment-list',
			'search-form',
			'comment-form'
		) );
	}

	// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
	function filter_ptags_on_images( $content ) {
		return preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
	}

	// This removes the annoying […] to a Read More link
	function excerpt_more( $more ) {
		global $post;

		// edit here if you like
		return '...  <a class="" href="' . get_permalink( $post->ID ) . '">' . 'Read more &raquo;' . '</a>';
	}


	function cosmic_scripts_load_cdn() {
		// Deregister the included library
		wp_deregister_script( 'jquery' );

		// Register the library again from Google's CDN
		wp_register_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js', false, null, true );
		wp_register_script( 'owl', '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/owl.carousel.min.js', false, null, true );
		wp_register_script( 'main', get_stylesheet_directory_uri() . '/js/main.js', false, null, true );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'owl' );
		wp_enqueue_script( 'main' );

		// Register the script like this for a theme:
		//wp_register_script( 'custom-script', get_template_directory_uri() . '/js/custom-script.js', array( 'jquery' ) );

		// For either a plugin or a theme, you can then enqueue the script:
		//wp_enqueue_script( 'custom-script' );
	}

	add_action( 'wp_enqueue_scripts', 'cosmic_scripts_load_cdn' );

	function scripts_and_styles() {
		wp_register_style( 'font-awesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' );
		wp_register_style( 'style', get_stylesheet_directory_uri() . '/style.css' );
		wp_enqueue_style( 'font-awesome' );
		wp_enqueue_style( 'style' );
	}


	//////////////////////////////////////
// Creating the widget
	class wpb_widget extends WP_Widget {

		function __construct() {
			parent::__construct(
// Base ID of your widget
				'wpb_widget',

// Widget name will appear in UI
				__( 'WPBeginner Widget', 'wpb_widget_domain' ),

// Widget description
				array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain' ), )
			);
		}

// Creating widget front-end
// This is where the action happens
		public function widget( $args, $instance ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
			echo $args['before_widget'];
			if ( ! empty( $title ) ) {
				echo $args['before_title'] . $title . $args['after_title'];
			}

// This is where you run the code and display the output
			echo __( 'Hello, World!', 'wpb_widget_domain' );
			echo $args['after_widget'];
		}

// Widget Backend
		public function form( $instance ) {
			if ( isset( $instance['title'] ) ) {
				$title = $instance['title'];
			} else {
				$title = __( 'New title', 'wpb_widget_domain' );
			}
// Widget admin form
			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
				       name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
				       value="<?php echo esc_attr( $title ); ?>"/>
			</p>
			<?php
		}

// Updating widget replacing old instances with new
		public function update( $new_instance, $old_instance ) {
			$instance          = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			return $instance;
		}
	} // Class wpb_widget ends here

// Register and load the widget
	function wpb_load_widget() {
		register_widget( 'wpb_widget' );
	}

	add_action( 'widgets_init', 'wpb_load_widget' );