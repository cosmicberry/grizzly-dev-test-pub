<?php
	/**
	 *
	 * Author: Cosmic Strawberry
	 * URL: http://cosmicstrawberry.com
	 *
	 */

	require_once( 'cosmic_includes/cosmic.php' );


	function cosmic_launch() {

		//add_action( 'init', 'head_cleanup' );
		head_cleanup();
		add_action( 'wp_enqueue_scripts', 'scripts_and_styles', 999 );
		add_filter( 'the_generator', 'rss_version' );
		add_filter( 'excerpt_more', 'excerpt_more' );
		add_filter( 'the_content', 'filter_ptags_on_images' );

		theme_support();

	}

	add_action( 'wp_loaded', 'cosmic_launch' );


	add_filter( 'the_content', 'do_shortcode' );

