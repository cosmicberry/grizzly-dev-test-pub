# Grizzly Front-End Dev Evaluation

This evaluation is meant to explore your individual development style and technique through the development of a simple landing page. We are looking for a faithfully styled responsive realization of the included comp. Additionally, this landing page should be developed as a functioning WordPress install. Please feel free to use whichever theme or HTML framework/scaffold you feel most comfortable using. The use of a framework or scaffold is not a requirement of this project

**Please limit yourself to 8 hours total ** when completing this evaluation. If you haven't quite finished the project by end of the alotted 8 hours, please put your pencil down and commit your code. All non-photo design elements should be created with CSS (SCSS). Please note that attention to detail is paramount in this exercise, so please be sure that the end product has been reviewed for responsive support and precision.

To begin, clone this repo down to your local directory and work from there. When you have completed your test, push your code up to a private BitBucket or GitHub repository and provide access to the Grizzly team (rich@madebygrizzly.com). Please make sure to include the working database in your repo for our review. Finally, once you've completed this project, deploy your code to a live staging environment of your choice and provide a link for our review.

As with any project, we believe that communication is a vital part of this test. If you have questions, concerns, or feedback please feel free to reach out to rich@madebygrizzly.com and we'll work with you to clarify and answer questions. 

#### Requirements
- The final deliverable is a functioning WordPress site.
- The theme should be built using SCSS, HTML5, PHP, and JavaScript.
- All of the assets you will need for this project are included in the design-assets folder.
- Font Awesome should be used for all icons.
- The fonts used in this project are both Google fonts: Ubuntu (Bold and Regular) and Lato (Black).
- The carousel should should display a real Instagram feed. Please don't use an existing WordPress plugin for this functionality and instead created your own solution. Hint: we typically use [this PHP Class](https://github.com/cosenary/Instagram-PHP-API) to interact with the Instagram API.

# Additional Considerations:

#### WordPress Framework/Plugin Considerations
Given the time-sensitive nature of this project, we encourage you to utilize the WordPress starter or framework you are most comfortable with (we love Sage by Roots.io, but are also big fans of underscores and bones). All styles and scripts should be properly enqueued. Using some sort of asset pipeline (Gulp/Grunt/CodeKit) is encouraged.

#### Responsive Considerations
When building out the front end, please ensure that your end-product is as visually put-together as possible. In addition to developing a beautiful desktop experience, we're also very much looking at how you choose to implement responsive design to maximize the experience on smaller (and larger) screens.

#### Instagram carousel
Feel free to use the JavaScript/jQuery plugin of your choice (ours is Owl Carousel 2 currently) and, again, avoid any sort of pre-built WordPress carousel plugin. Please ensure that the carousel consists of 8 images on an endless loop, linking out to the original Instagram post.

#### Form Styling
The form should be styled exactly the way it is seen in the comp, and should not use any default browser or device styling. 

# Evaluatory Criteria

#### Code == Design
The implementation of design on the web should never sacrifice that design's integrity. Development is about bringing design to life, and our developers are as integral to this process as our designers. Dev shouldn't be restrictive, and we encourage our developers to work with our designers and strategists to create amazing interactive experiences. This means that you are invited to breathe life and excitement into the page through interaction and attention to detail.

#### Attention to Detail
This is huge. We expect that all code written for/by us has been tested, is cross-browser compatible, and is optimized for the best user experience. Assets should be minified/compressed, code should be modular and scalable, and the intent of the design should be reflected in the final deliverable.

#### Code Quality
This is another huge component of the Grizzly ethos. We will be looking through every line of submitted code, and will be looking for best-practice-caliber PHP, JavaScript, Sass, and HTML. Keep your code clean, readable, well-organized, and document/comment when necessary.

#### Commit Quality
We share code a lot here, and as such it's very important to commit often and to include informative, helpful commit messages. This makes everyone's job easier down the road, and it certainly makes tracking down a bug or error much much less cumbersome.

#### Grizzly Vibes
We get things done, but we have fun. We are a community of open communication. If something is unclear, please ask questions. Most importantly enjoy what you do, have fun, and happy coding!